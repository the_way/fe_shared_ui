declare module "*.jpg" {
  const value: string;
  export = value;
}

declare module "*.png" {
  const value: string;
  export = value;
}

declare module "*.svg" {
  import type { ReactElement, SVGProps } from "react";

  const content: (props: SVGProps<SVGElement>) => ReactElement;
  export default content;
}

type PickFromUnion<T, U extends T> = T extends U ? T : never;
type AutoOmit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
