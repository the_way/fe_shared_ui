// import React from "react";
import { CssBaseline, ThemeProvider } from "@mui/material";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
// TODO: (AM) public api
import { theme } from "shared/config/theme";
import { Photo, TelegramIcon } from "shared/ui";

const App = () => (
  <ThemeProvider theme={theme}>
    <CssBaseline />

    <BrowserRouter>
      <div>shared_ui module</div>
      <Photo />
      <TelegramIcon />
    </BrowserRouter>
  </ThemeProvider>
);
ReactDOM.render(<App />, document.getElementById("app"));
