// eslint-disable-next-line import/no-extraneous-dependencies
import type { ComponentStory } from "@storybook/react";

import type { TAllPropsStory, TOnePropStory } from "./types";
import { ItemsContainer } from "./ui";

export const createStoryFactory = <Props,>(
  Component: (props: Props) => JSX.Element
) => {
  const createAllPropsStory = ({ storyName, props }: TAllPropsStory<Props>) => {
    const Story: ComponentStory<typeof Component> = (args) => {
      let componentsProps: Array<Props> = props;

      return (
        <ItemsContainer
          componentsProps={componentsProps}
          Component={Component}
          baseProps={args}
        />
      );
    };
    Story.storyName = storyName;

    return Story;
  };

  const createOnePropStory = <
    PropKey extends keyof Props,
    PropValues extends Props[PropKey]
  >({
    storyName,
    propName,
    values,
  }: TOnePropStory<Props, PropKey, PropValues>) => {
    const Story: ComponentStory<typeof Component> = (args) => {
      let componentsProps: Array<Props> = values.map(
        (value) => ({ [propName!]: value } as unknown as Props)
      );

      return (
        <ItemsContainer
          componentsProps={componentsProps}
          Component={Component}
          baseProps={args}
        />
      );
    };

    Story.storyName = storyName;

    return Story;
  };
  return { createAllPropsStory, createOnePropStory };
};
