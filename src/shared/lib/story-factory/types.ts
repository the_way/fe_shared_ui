export type TAllPropsStory<Props> = {
  storyName?: string;
  props: Array<Props>;
};

export type TOnePropStory<
  Props,
  PropKey extends keyof Props,
  PropValues extends Props[PropKey]
> = {
  storyName?: string;
  propName: PropKey;
  values: Array<PropValues>;
};

export type TItemComponentProps<Props> = {
  Component: (props: Props) => JSX.Element;
  storyProps: Props;
  baseProps: Props;
};
