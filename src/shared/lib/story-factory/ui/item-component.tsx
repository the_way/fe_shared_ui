import { styled } from "@mui/material";
import type { TItemComponentProps } from "../types";

import { PropName } from "./props-name";

export const StyledItemComponent = styled("div")(({ theme }) => ({
  marginBottom: theme.spacing(5),
}));

export const ItemComponent = <Props,>({
  Component,
  storyProps,
  baseProps,
}: TItemComponentProps<Props>) => {
  return (
    <StyledItemComponent>
      {Object.entries(storyProps).map(([propName, value], idx) => (
        <PropName propName={propName} value={value as unknown} key={idx} />
      ))}
      <Component {...baseProps} {...storyProps} />
    </StyledItemComponent>
  );
};
