import { Box, styled } from "@mui/material";
import type { TItemComponentProps } from "../types";
import { ItemComponent } from "./item-component";

export const StyledItemsContainer = styled(Box)(() => ({
  display: "grid",
}));

export const ItemsContainer = <Props,>({
  Component,
  baseProps,
  componentsProps,
}: Pick<TItemComponentProps<Props>, "Component" | "baseProps"> & {
  componentsProps: Props[];
}) => {
  return (
    <>
      {componentsProps.map((storyProps, i) => (
        <ItemComponent
          Component={Component}
          baseProps={baseProps}
          storyProps={storyProps}
          key={i}
        />
      ))}
    </>
  );
};
