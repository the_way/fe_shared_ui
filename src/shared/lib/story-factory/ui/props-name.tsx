import { styled } from "@mui/material";
import type { FC } from "react";

const StyledPropName = styled("p")`
  text-align: left;
  color: ${({ theme }) => theme.palette.grey[700]};
  margin: 0 0 ${({ theme }) => theme.spacing(1.5)};
  font-size: ${({ theme }) => theme.typography.body2.fontSize};
`;

type TProps = {
  propName?: string;
  value?: unknown;
  noProps?: boolean;
};

export const PropName: FC<TProps> = ({ propName, value, noProps }) => {
  if (noProps) {
    return <StyledPropName>Default</StyledPropName>;
  }

  let textValue;

  if (value === undefined) {
    textValue = "undefined";
  } else if (value === null) {
    textValue = "null";
  } else if (value instanceof Date) {
    textValue = "new Date()";
  } else if (
    typeof value === "string" ||
    typeof value === "number" ||
    typeof value === "boolean"
  ) {
    textValue = JSON.stringify(value);
  } else {
    textValue = "...";
  }

  return (
    <StyledPropName>
      <b>{propName}</b>: <i>{textValue}</i>
    </StyledPropName>
  );
};
