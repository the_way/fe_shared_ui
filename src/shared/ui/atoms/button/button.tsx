import { Button as MuiButton, ButtonProps, styled } from "@mui/material";

export type TButtonProps = ButtonProps;

export const StyledMuiButton = styled(MuiButton)(() => ({
  textTransform: "none",
}));

export const Button = ({
  size = "small",
  variant = "outlined",
  ...rest
}: TButtonProps) => {
  return <StyledMuiButton size={size} variant={variant} {...rest} />;
};
