import { Delete as DeleteIcon } from "@mui/icons-material";
import type { ComponentMeta } from "@storybook/react";
import { createStoryFactory } from "../../../lib/story-factory";

import { Button } from "./button";

export default {
  title: "UI atoms/Button",
  component: Button,
  args: {
    children: "Button text",
  },

  parameters: {
    componentSource: {
      code: Button,
      language: "typescript",
    },
  },
} as ComponentMeta<typeof Button>;

const { createAllPropsStory, createOnePropStory } = createStoryFactory(Button);

export const Variant = createOnePropStory({
  propName: "variant",
  values: ["contained", "outlined"],
});
export const Color = createAllPropsStory({
  props: [
    { variant: "outlined", color: "error" },
    { variant: "outlined", color: "info" },
    { variant: "outlined", color: "primary" },
    { variant: "outlined", color: "secondary" },
    { variant: "contained", color: "error" },
    { variant: "contained", color: "info" },
    { variant: "contained", color: "primary" },
    { variant: "contained", color: "secondary" },
  ],
});

export const Size = createOnePropStory({
  propName: "size",
  values: ["small", "medium", "large"],
});

export const Icon = createAllPropsStory({
  props: [
    {
      startIcon: <DeleteIcon />,
    },
    {
      startIcon: <DeleteIcon />,
    },
    {
      endIcon: <DeleteIcon />,
    },
  ],
});

export const Disabled = createOnePropStory({
  propName: "disabled",
  values: [false, true],
});
