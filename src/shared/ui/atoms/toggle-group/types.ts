import type { ToggleButtonGroupProps } from "@mui/material";
import type { ReactElement } from "react";

export type TToggleItemProps =
  | {
      value: string;
      Icon: ReactElement;
      ariaLabel?: string;
      label?: string;
    }
  | {
      value: string;
      ariaLabel?: string;
      label: string;
      Icon?: ReactElement;
    };

export type TToggleButtonGroupProps = ToggleButtonGroupProps & {
  /**
   * toggle buttons data
   */
  data: TToggleItemProps[];
  /**
   * at least one button must be active if true
   */
  shouldEnforceValueSet?: boolean;
};

export type TValue = string | string[];
