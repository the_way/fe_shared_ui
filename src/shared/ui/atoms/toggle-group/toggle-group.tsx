import { ToggleButtonGroup } from "@mui/material";
import type { MouseEvent } from "react";
import { useCallback, useState } from "react";
import { StyledToggleButton } from "./styles";
import type { TToggleButtonGroupProps, TValue } from "./types";

export const ToggleGroup = ({
  value: propValue,
  onChange,
  data,
  shouldEnforceValueSet,
  exclusive = false,
  color,
  ...props
}: TToggleButtonGroupProps) => {
  const useStateDefaultValue = () => {
    if (exclusive) {
      return shouldEnforceValueSet ? data[0].value : "null";
    }
    return shouldEnforceValueSet ? [data[0].value] : [];
  };

  const [value, setValue] = useState<TValue>(propValue || useStateDefaultValue);

  const handleToggleChange = useCallback(
    (event: MouseEvent<HTMLElement>, newValue: TValue) => {
      const hasValue =
        newValue instanceof Array
          ? Boolean(newValue.length)
          : newValue !== null;
      if (!shouldEnforceValueSet || hasValue) {
        setValue(newValue);
        if (onChange) onChange(event, newValue);
      }
    },
    [onChange, shouldEnforceValueSet]
  );

  return (
    <ToggleButtonGroup
      value={value}
      onChange={handleToggleChange}
      exclusive={exclusive}
      {...props}
    >
      {data.map((item, idx: number) => (
        <StyledToggleButton
          value={item.value}
          aria-label={item.ariaLabel}
          key={idx}
        >
          {item.Icon}
          {item.label}
        </StyledToggleButton>
      ))}
    </ToggleButtonGroup>
  );
};
