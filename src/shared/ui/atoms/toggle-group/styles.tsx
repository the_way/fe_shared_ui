import { styled, ToggleButton } from "@mui/material";

export const StyledToggleButton = styled(ToggleButton)(({ theme }) => ({
  textTransform: "none",
  backgroundColor: "white",
  ":hover": {
    backgroundColor: "white",
    color: "black",
  },
  "&.MuiButtonBase-root.MuiToggleButton-root.Mui-selected.MuiToggleButton-standard.MuiToggleButtonGroup-grouped":
    {
      backgroundColor: theme.palette.grey[200],
    },
}));
