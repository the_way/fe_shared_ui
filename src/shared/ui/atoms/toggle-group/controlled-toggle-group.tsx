import type { ComponentProps } from "react";
import { Control, Controller, FieldPath, FieldValues } from "react-hook-form";
import { ToggleGroup } from "./toggle-group";

type TControlledToggleGroupProps<
  TFieldValues extends FieldValues = FieldValues,
  TName extends FieldPath<TFieldValues> = FieldPath<TFieldValues>
> = {
  name: TName;
  control: Control<TFieldValues>;
  required?: boolean;
  elementProps: ComponentProps<typeof ToggleGroup>;
};

export const ControlledToggleGroup = <T,>({
  name,
  required,
  control,
  elementProps,
}: TControlledToggleGroupProps<T>) => {
  return (
    <Controller
      name={name}
      control={control}
      rules={{
        required,
      }}
      render={({
        field: { value, onBlur, onChange },
        fieldState: { error: { message } = {} },
      }) => (
        <ToggleGroup
          {...elementProps}
          //   data={[{ value, ariaLabel: value }]}
          onChange={onChange}
          onBlur={onBlur}
        />
      )}
    />
  );
};
