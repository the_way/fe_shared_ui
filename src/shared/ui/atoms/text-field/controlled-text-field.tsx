import type { ComponentProps } from "react";
import { Control, Controller, FieldValues } from "react-hook-form";
import { TextField } from "./text-field";

type TControlledTextFieldProps = {
  name: string;
  required?: boolean;
  elementProps?: ComponentProps<typeof TextField>;
  control: Control<FieldValues, any>;
};

export const ControlledTextField = ({
  name,
  required,
  control,
  elementProps,
}: TControlledTextFieldProps) => {
  return (
    <Controller
      name={name}
      control={control}
      rules={{
        required,
      }}
      render={({
        field: { value, onBlur, onChange },
        fieldState: { invalid, error: { message } = {} },
      }) => (
        // TODO: (AM) ignore ????
        // @ts-ignore
        <TextField
          {...elementProps}
          value={value as string | number | undefined}
          onChange={onChange}
          onBlur={onBlur}
          required
          error={invalid}
          helperText={message || elementProps?.helperText}
        />
      )}
    />
  );
};
