import { TextField as MuiTextField, TextFieldProps } from "@mui/material";

export const TextField = (props: TextFieldProps) => {
  return (
    <MuiTextField
      id="filled-multiline-flexible"
      label="Multiline"
      multiline
      maxRows={4}
      variant="filled"
      {...props}
    />
  );
};
