import type { ComponentProps } from "react";
import { Control, Controller, FieldPath, FieldValues } from "react-hook-form";
import { Input } from "./input";

type TControlledInputProps<
  TFieldValues extends FieldValues = FieldValues,
  TName extends FieldPath<TFieldValues> = FieldPath<TFieldValues>
> = {
  name: TName;
  control: Control<TFieldValues>;
  required?: boolean;
  elementProps?: ComponentProps<typeof Input>;
};

export const ControlledInput = <T,>({
  name,
  required,
  control,
  elementProps,
}: TControlledInputProps<T>) => {
  return (
    <Controller
      name={name}
      control={control}
      rules={{
        required,
      }}
      render={({
        field: { value, onBlur, onChange },
        fieldState: { invalid, error: { message } = {} },
      }) => (
        // TODO: (AM) ignore ????
        // @ts-ignore
        <Input
          {...elementProps}
          value={value as string | number | undefined}
          onChange={onChange}
          onBlur={onBlur}
          required
          error={invalid}
          // helperText={message || elementProps?.helperText}
        />
      )}
    />
  );
};
