import { Rating as MuiRating } from "@mui/material";
import Box from "@mui/material/Box";
import { Body2 } from "src/remote";
import { StyledCircleIcon } from "./styles";

type TProps = {
  value: number;
  LABELS: { [index: number]: string };
};

export const Rating = ({ value, LABELS }: TProps) => {
  return (
    <Box
      sx={{
        width: 200,
        display: "flex",
        alignItems: "center",
      }}
    >
      <MuiRating
        name="skill-rate"
        value={value}
        precision={1}
        max={4}
        icon={<StyledCircleIcon />}
        emptyIcon={
          <StyledCircleIcon style={{ opacity: 0.25 }} fontSize="inherit" />
        }
        readOnly
      />
      {value !== null && (
        <Box sx={{ ml: 2 }}>
          <Body2> {LABELS[value]}</Body2>
        </Box>
      )}
    </Box>
  );
};
