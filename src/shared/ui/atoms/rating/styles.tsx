import CircleIcon from "@mui/icons-material/Circle";
import { styled } from "@mui/material";

export const StyledCircleIcon = styled(CircleIcon)(({ theme }) => ({
  boxShadow: theme.shadows[1],
  borderRadius: "50%",
  color: theme.palette.primary.main,
}));
