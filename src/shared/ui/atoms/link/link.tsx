import { StyledA, StyledLink, StyledNavLink } from "./styles";
import type { TLinkProps } from "./types";

export const Link = ({
  children,
  to,
  isExternal = false,
  isNavLink,
  fontSize = 20,
  className,
  ...props
}: TLinkProps) => {
  console.log("props", props);
  if (isExternal) {
    return (
      <StyledA
        href={to}
        target="_blank"
        fontSize={fontSize}
        className={className}
      >
        {children}
      </StyledA>
    );
  }
  if (isNavLink) {
    <StyledNavLink to={to} fontSize={fontSize} className={className}>
      {children}
    </StyledNavLink>;
  }
  return (
    <StyledLink to={to} fontSize={fontSize} className={className}>
      {children}
    </StyledLink>
  );
};
