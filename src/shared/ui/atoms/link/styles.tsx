import { styled, Theme } from "@mui/material";
import type { CSSProperties } from "@mui/styled-engine";
import { Link, NavLink } from "react-router-dom";

const commonStyles = (theme: Theme) =>
  ({
    textDecoration: "none",
    color: "darkgray",
  } as CSSProperties);

type TLinksProps = {
  fontSize: number;
};

const shouldForwardProp = (prop: string) => prop !== "fontSize";

export const StyledLink = styled(Link, {
  shouldForwardProp,
})<TLinksProps>(({ theme, fontSize }) => ({
  ...commonStyles(theme),
  fontSize,
}));

export const StyledNavLink = styled(NavLink, {
  shouldForwardProp,
})<TLinksProps>(({ theme, fontSize }) => ({
  ...commonStyles(theme),
  fontSize,
}));

export const StyledA = styled("a", { shouldForwardProp })<TLinksProps>(
  ({ theme, fontSize }) => ({
    ...commonStyles(theme),
    fontSize,
  })
);
