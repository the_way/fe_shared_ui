export type TLinkProps = {
  children: string;
  to: string;
  fontSize?: number;
  className?: string;
} & (
  | { isNavLink?: boolean; isExternal?: false }
  | { isExternal?: boolean; isNavLink?: false }
);
