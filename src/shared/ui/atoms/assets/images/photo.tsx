import { Box, BoxProps } from "@mui/material";
import jpgPhoto from "./photo.jpg";
export const Photo = (prop: BoxProps) => (
  <Box {...prop}>
    <img src={jpgPhoto} width="300px" />
  </Box>
);
