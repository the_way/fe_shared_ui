import { Box, BoxProps } from "@mui/material";
import jpgPhoto from "./mltn.jpg";

export const Mltn = (prop: BoxProps) => (
  <Box {...prop}>
    <img src={jpgPhoto} width="100px" />
  </Box>
);
