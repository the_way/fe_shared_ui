import { Box, BoxProps } from "@mui/material";
import jpgPhoto from "./mtshv.jpg";
export const Mtshv = (prop: BoxProps) => (
  <Box {...prop}>
    <img src={jpgPhoto} width="100px" />
  </Box>
);
