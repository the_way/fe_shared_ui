import type { ChipProps } from "@mui/material";
import { StyledMuiChip } from "./styles";

export type TProps = {
  size: ChipProps["size"] | "large";
} & Omit<ChipProps, "size">;

export const Chip = ({ size, ...rest }: TProps) => {
  return (
    <StyledMuiChip
      variant="outlined"
      size={size === "large" ? "medium" : size}
      isLargeSize={size === "large"}
      {...rest}
    />
  );
};
