import { Chip, styled } from "@mui/material";
import type { TProps } from "./chip";

export const StyledMuiChip = styled(Chip)<TProps & { isLargeSize: boolean }>(
  ({ theme, isLargeSize }) => ({
    ...(isLargeSize && { height: "40px", fontSize: "20px" }),
    margin: theme.spacing(0.2),
  })
);
