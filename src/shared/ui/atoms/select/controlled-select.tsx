import type { ComponentProps } from "react";
import { Control, Controller, FieldValues } from "react-hook-form";
import { Select } from "./select";

type TControlledSelectProps = {
  name: string;
  required?: boolean;
  elementProps?: ComponentProps<typeof Select>;
  control: Control<FieldValues, any>;
};

export const ControlledSelect = ({
  name,
  required,
  control,
  elementProps,
}: TControlledSelectProps) => {
  return (
    <Controller
      name={name}
      control={control}
      rules={{
        required,
      }}
      render={({
        field: { value, onBlur, onChange },
        fieldState: { error: { message } = {} },
      }) => (
        <Select
          {...elementProps}
          value={value as string | number | undefined}
          onChange={onChange}
          onBlur={onBlur}
          required
          error={Boolean(message)}
        />
      )}
    />
  );
};
