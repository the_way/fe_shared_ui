import { Box, BoxProps, styled } from "@mui/material";

type TProps = {
  xl?: BoxProps;
  lg?: BoxProps;
  xs?: BoxProps;
  md?: BoxProps;
  sm?: BoxProps;
};

export const Grid = styled(Box)<TProps>(({ theme, xs, sm, md, lg, xl }) => ({
  display: "grid",

  [theme.breakpoints.up("xs")]: {
    ...xs,
  },
  [theme.breakpoints.up("sm")]: {
    ...sm,
  },
  [theme.breakpoints.up("md")]: {
    ...md,
  },
  [theme.breakpoints.up("lg")]: {
    ...lg,
  },
  [theme.breakpoints.up("lg")]: {
    ...xl,
  },
}));
