import { Typography as MuiTypography, TypographyProps } from "@mui/material";

export const BaseTypography = (props: TypographyProps) => {
  return <MuiTypography variant="body1" {...props} />;
};
