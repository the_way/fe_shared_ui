import { IconButton as MuiIconButton, IconButtonProps } from "@mui/material";
import type { CSSProperties } from "@mui/styled-engine";

export const IconButton = (
  props: IconButtonProps & { cursor?: CSSProperties["cursor"] }
) => {
  return <MuiIconButton {...props} />;
};
