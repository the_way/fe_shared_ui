import { Box } from "@mui/material";
import { NavLink } from "react-router-dom";

import { StyledMenuTitle } from "./styles";
import type { TMenuTitleItemProps } from "./types";

export const MenuTitleItem = ({
  titleItem,
  showTitleIcon,
}: TMenuTitleItemProps) => {
  if (!titleItem) {
    return null;
  }
  console.log("showTitleIcon", showTitleIcon);
  const { Icon } = titleItem;
  if (titleItem && titleItem.type === "link") {
    return (
      <StyledMenuTitle>
        {showTitleIcon && Icon && <Box>{Icon}</Box>}
        <NavLink
          end
          to={titleItem.to}
          // style={(e) => navLinkStyle({ ...e,  })}
        >
          {titleItem.children}
        </NavLink>
      </StyledMenuTitle>
    );
  }

  return (
    <StyledMenuTitle>
      {showTitleIcon && Icon && (
        <Box height="24px" marginRight={1}>
          {Icon}
        </Box>
      )}
      {titleItem.text}
    </StyledMenuTitle>
  );
};
