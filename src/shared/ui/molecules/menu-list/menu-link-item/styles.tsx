import { MenuItem, styled } from "@mui/material";
import { Link } from "shared/ui/atoms";
import type { TMenuListProps } from "../types";

export const StyledMenuItem = styled(MenuItem, {
  shouldForwardProp: (propName) => "itemsPaddinsLeft" !== String(propName),
})<Pick<TMenuListProps, "itemsPaddinsLeft">>(
  ({ theme, itemsPaddinsLeft = 2 }) => ({
    padding: 0,
    margin: "0px",
    backgroundColor: "white",
    fontSize: theme.typography.caption.fontSize,
    "&:hover": {
      backgroundColor: theme.palette.grey[100],
    },
  })
);

export const StyledLink = styled(Link, {
  shouldForwardProp: (propName) => "itemsPaddinsLeft" !== String(propName),
})<Pick<TMenuListProps, "itemsPaddinsLeft">>(
  ({ theme, itemsPaddinsLeft = 2 }) => ({
    display: "block",
    width: "100%",
    padding: theme.spacing(1),
    paddingLeft: theme.spacing(itemsPaddinsLeft),
  })
);
