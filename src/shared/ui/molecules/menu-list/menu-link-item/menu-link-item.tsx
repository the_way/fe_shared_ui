import { StyledLink, StyledMenuItem } from "./styles";
import type { TMenuLinkItemProps } from "../types";

export const MenuLinkItem = ({
  item,
  itemsPaddinsLeft,
}: TMenuLinkItemProps) => {
  return (
    <StyledMenuItem>
      <StyledLink itemsPaddinsLeft={itemsPaddinsLeft} {...item}>
        {item.children}
      </StyledLink>
    </StyledMenuItem>
  );
};
