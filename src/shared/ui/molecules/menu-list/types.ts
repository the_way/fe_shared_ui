import type { ReactElement } from "react";
import type { TLinkProps } from "../../atoms/link";

export type TMenuListProps = {
  items?: TLinkProps[];
  titleItem?:
    | { type: "text"; text: string; Icon: ReactElement }
    | (TLinkProps & { type: "link"; Icon: ReactElement });
  showTitleIcon?: boolean;
  itemsPaddinsLeft?: number;
  width?: string;
};

export type TMenuLinkItemProps = { item: TLinkProps } & Pick<
  TMenuListProps,
  "itemsPaddinsLeft"
>;

export type TMenuTitleItemProps = Required<
  Pick<TMenuListProps, "titleItem" | "showTitleIcon">
>;
