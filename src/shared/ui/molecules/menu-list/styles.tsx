import { MenuItem, MenuList, styled } from "@mui/material";
import type { TMenuListProps } from "./types";

export const StyledMenuList = styled(MenuList)<{ width: string }>(
  ({ theme, width }) => ({
    paddingTop: 0,
    paddingBottom: 0,
    boxShadow: "none",
    width,
  })
);

export const StyledMenuItem = styled(MenuItem, {
  shouldForwardProp: (propName) => "itemsPaddinsLeft" !== String(propName),
})<Pick<TMenuListProps, "itemsPaddinsLeft">>(
  ({ theme, itemsPaddinsLeft = 2 }) => ({
    padding: theme.spacing(1),
    paddingLeft: theme.spacing(itemsPaddinsLeft),
    margin: "0px",
    backgroundColor: "white",
    fontSize: theme.typography.caption.fontSize,
    "&:hover": {
      backgroundColor: theme.palette.grey[100],
    },
  })
);

export const StyledMenuTitle = styled(MenuItem)(({ theme }) => ({
  padding: theme.spacing(1),
  paddingLeft: theme.spacing(2),
  margin: 0,
  backgroundColor: "white",
  fontWeight: "bold",
  cursor: "default",
}));
