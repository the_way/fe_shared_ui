import { memo } from "react";

import { MenuLinkItem } from "./menu-link-item";
import { MenuTitleItem } from "./menu-title-item";
import { StyledMenuList } from "./styles";
import type { TMenuListProps } from "./types";

export const MenuList = memo(
  ({
    titleItem,
    items,
    showTitleIcon = false,
    itemsPaddinsLeft = 3,
    width = "240px",
  }: TMenuListProps) => {
    return (
      <StyledMenuList width={width}>
        {titleItem && (
          <MenuTitleItem titleItem={titleItem} showTitleIcon={showTitleIcon} />
        )}
        {items &&
          items[0] &&
          items.map((item) => {
            return (
              <MenuLinkItem
                item={item}
                key={item.to}
                itemsPaddinsLeft={itemsPaddinsLeft}
              />
            );
          })}
      </StyledMenuList>
    );
  }
);
