import type { TypographyProps } from "@mui/material";
import { BaseTypography } from "shared/ui/atoms";

export const H2 = (props: TypographyProps) => {
  return <BaseTypography variant="h2" marginY={5} {...props} />;
};
