import type { TypographyProps } from "@mui/material";
import { BaseTypography } from "shared/ui/atoms";

export const Subtitle2 = (props: TypographyProps) => {
  return <BaseTypography variant="subtitle2" {...props} />;
};
