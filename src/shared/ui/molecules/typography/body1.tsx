import type { TypographyProps } from "@mui/material";
import { BaseTypography } from "shared/ui/atoms";

export const Body1 = (props: TypographyProps) => {
  return <BaseTypography variant="body1" marginTop={1} {...props} />;
};
