import type { TypographyProps } from "@mui/material";
import { BaseTypography } from "shared/ui/atoms";

export const Caption = (props: TypographyProps) => {
  return <BaseTypography variant="caption" {...props} />;
};
