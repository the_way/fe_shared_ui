import type { TypographyProps } from "@mui/material";
import { BaseTypography } from "shared/ui/atoms";

export const H5 = (props: TypographyProps) => {
  return <BaseTypography variant="h5" marginTop={3} {...props} />;
};
