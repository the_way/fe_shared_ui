import type { TypographyProps } from "@mui/material";
import { BaseTypography } from "shared/ui/atoms";

export const H1 = (props: TypographyProps) => {
  return <BaseTypography variant="h1" marginBottom={6} {...props} />;
};
