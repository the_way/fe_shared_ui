import type { TypographyProps } from "@mui/material";
import { BaseTypography } from "shared/ui/atoms";

export const H4 = (props: TypographyProps) => {
  return (
    <BaseTypography
      variant="h4"
      justifySelf="center"
      marginTop="50px"
      {...props}
    />
  );
};
