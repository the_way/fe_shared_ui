import type { TypographyProps } from "@mui/material";
import { BaseTypography } from "shared/ui/atoms";

export const H6 = (props: TypographyProps) => {
  return <BaseTypography variant="h6" marginTop={3} {...props} />;
};
