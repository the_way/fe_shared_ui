import type { TypographyProps } from "@mui/material";
import { BaseTypography } from "shared/ui/atoms";

export const Subtitle1 = (props: TypographyProps) => {
  return <BaseTypography variant="subtitle1" {...props} />;
};
