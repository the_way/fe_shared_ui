import type { TypographyProps } from "@mui/material";
import { BaseTypography } from "shared/ui/atoms";

export const H3 = (props: TypographyProps) => {
  return <BaseTypography variant="h3" marginTop={8} {...props} />;
};
