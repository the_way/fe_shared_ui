import type { TypographyProps } from "@mui/material";
import { BaseTypography } from "shared/ui/atoms";

export const Body2 = (props: TypographyProps) => {
  return <BaseTypography variant="body2" {...props} />;
};
