import { Card, styled } from "@mui/material";
import { theme } from "shared/config/theme";
import { IconButton } from "../../atoms";
import type { ExpandMoreProps } from "./types";

export const StyledMuiCard = styled(Card)(() => ({
  minWidth: 345,
  width: "80vw",
  marginTop: theme.spacing(4),
}));

export const ExpandMore = styled((props: ExpandMoreProps) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? "rotate(0deg)" : "rotate(180deg)",
  marginLeft: "auto",
  transition: theme.transitions.create("transform", {
    duration: theme.transitions.duration.shortest,
  }),
}));
