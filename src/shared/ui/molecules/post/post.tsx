import CardHeader from "@mui/material/CardHeader";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import CardActions from "@mui/material/CardActions";
import Collapse from "@mui/material/Collapse";
import IconButton from "@mui/material/IconButton";
import FavoriteIcon from "@mui/icons-material/Favorite";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { ExpandMore, StyledMuiCard } from "./styles";

import { useState } from "react";
import type { TFeed } from "./types";
import { BaseTypography } from "shared/ui/atoms";

export const Post = ({
  title,
  shortDescription,
  fullDescription,
  category,
}: TFeed) => {
  const [expanded, setExpanded] = useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <StyledMuiCard>
      <CardHeader
        title={title}
        subheader={new Date().toDateString()}
        avatar={category}
      />
      <CardMedia
        component="img"
        height="250"
        image={`https://picsum.photos/1200/600/?random${title}`}
        alt={title}
      />
      <CardContent>
        <BaseTypography variant="body2" color="text.secondary">
          {shortDescription}
        </BaseTypography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton>
        <ExpandMore
          expand={expanded}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <BaseTypography paragraph>{fullDescription}</BaseTypography>
        </CardContent>
      </Collapse>
    </StyledMuiCard>
  );
};
