import type { IconButtonProps } from "@mui/material";

export type ExpandMoreProps = IconButtonProps & {
  expand: boolean;
};
export type TFeed = {
  title: string;
  shortDescription: string;
  fullDescription: string;
  category: string;
};