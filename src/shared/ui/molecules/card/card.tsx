import CardContent from "@mui/material/CardContent";
import Collapse from "@mui/material/Collapse";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { ExpandMore, StyledMuiCard } from "./styles";

import { useState } from "react";
import type { TCard } from "./types";
import { Box, List, ListItem } from "@mui/material";
import { Link } from "src/remote";

export const Card = ({
  children,
  collapseData,
  isOpenCollapse = false,
  collapseChildren = null,
}: TCard) => {
  const [expanded, setExpanded] = useState(isOpenCollapse);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <StyledMuiCard>
      <CardContent>
        <Box
          display="grid"
          gridAutoFlow="column"
          gridTemplateColumns="180px 1fr"
        >
          {children}
          {(collapseData || collapseChildren) && (
            <ExpandMore
              expand={expanded}
              onClick={handleExpandClick}
              aria-expanded={expanded}
              aria-label="show more"
            >
              <ExpandMoreIcon />
            </ExpandMore>
          )}
        </Box>
      </CardContent>

      {(collapseData || collapseChildren) && (
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent>
            {collapseChildren}
            {collapseData && (
              <List>
                {collapseData.map(({ to, title, isExternal = true }) => (
                  <ListItem>
                    <Link to={to} isExternal={isExternal}>
                      {title}
                    </Link>
                  </ListItem>
                ))}
              </List>
            )}
          </CardContent>
        </Collapse>
      )}
    </StyledMuiCard>
  );
};
