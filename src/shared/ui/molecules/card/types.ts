import type { IconButtonProps } from "@mui/material";
import type { ReactElement } from "react";

export type ExpandMoreProps = IconButtonProps & {
  expand: boolean;
};
export type TCard = {
  children: ReactElement;
  collapseData?: { title: string; to: string; isExternal?: boolean }[];
  isOpenCollapse?: boolean;
  collapseChildren?: ReactElement | null;
};
