import { List, styled } from "@mui/material";

export const StyledMuiSidebar = styled(List)(() => ({
  display: "grid",
  width: "100%",
  bgcolor: "background.paper",
}));
