import { List } from "@mui/material";
import type { CSSProperties } from "@mui/styled-engine";
import { memo, useMemo } from "react";

import type { TSidebarProps } from "../types";
import { SidebarItems } from "./sidebar-items";

export const Sidebar = memo(({ items, ...props }: TSidebarProps) => {
  const listStyle: CSSProperties = useMemo(
    () => ({
      bgcolor: "background.paper",
      height: "100vh",
    }),
    []
  );

  return (
    <List
      // sx because of error when using styled
      sx={listStyle}
      component="nav"
    >
      <SidebarItems items={items} {...props} />
    </List>
  );
});
