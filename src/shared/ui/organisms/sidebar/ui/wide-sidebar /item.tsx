import { MoreVert } from "@mui/icons-material";
import { memo, useMemo } from "react";

import type { TSidebarItemProps } from "../../types";
import { MenuList } from "shared/ui";
import type { TMenuTitleItemProps } from "shared/ui";
import { SIDEBAR_WIDTH } from "../../config";

export const WideSidebarItem = memo(
  ({
    menuTitle: { Icon = <MoreVert />, text, to },
    menuItems,
  }: TSidebarItemProps) => {
    const memoTitleItem: TMenuTitleItemProps["titleItem"] = useMemo(
      () => ({
        type: "text",
        text,
        Icon,
        to,
        isNavLink: Boolean(to),
      }),
      [text]
    );

    return (
      <MenuList
        titleItem={memoTitleItem}
        items={menuItems}
        showTitleIcon
        itemsPaddinsLeft={6}
        width={SIDEBAR_WIDTH}
      />
    );
  }
);
