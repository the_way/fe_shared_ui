import { MoreVert } from "@mui/icons-material";
import type { MouseEvent } from "react";
import { memo, useCallback, useMemo } from "react";
import { useNavigate } from "react-router-dom";

import { IconButton } from "shared/ui";

import type { TSidebarItemProps } from "../../types";
import { MenuList } from "shared/ui";
import { StyledTooltip, StyledItemBox } from "./styles";
import type { TMenuTitleItemProps } from "shared/ui/molecules";
import { SIDEBAR_WIDTH } from "../../config";

export const NarrowSidebarItem = memo(
  ({
    menuTitle: { Icon = <MoreVert />, text, to },
    menuItems,
  }: TSidebarItemProps) => {
    const navigate = useNavigate();

    //  prevent the bug when after clicking on the button (e.g. 'Settings'),
    //  tooltip doesn't respond to clicking on the link
    const handleMouseDown = (e: MouseEvent<HTMLDivElement>) => {
      e.stopPropagation();
      e.preventDefault();
    };

    const handleIconClick = useCallback(() => {
      if (to) {
        navigate(to);
      }
    }, [navigate, to]);

    const memoTitleItem: TMenuTitleItemProps["titleItem"] = useMemo(
      () => ({
        type: "text",
        text,
        Icon,
        to,
        isNavLink: Boolean(to),
      }),
      [text]
    );

    const tooltipContent = useMemo(
      () => (
        <MenuList
          titleItem={memoTitleItem}
          items={menuItems}
          width={SIDEBAR_WIDTH}
        />
      ),
      []
    );

    return (
      <StyledItemBox>
        <StyledTooltip
          title={tooltipContent}
          placement="right-start"
          PopperProps={{
            onMouseDown: handleMouseDown,
          }}
          enterDelay={0}
        >
          <div>
            <IconButton cursor={"unset"} onClick={handleIconClick}>
              {Icon}
            </IconButton>
          </div>
        </StyledTooltip>
      </StyledItemBox>
    );
  }
);
