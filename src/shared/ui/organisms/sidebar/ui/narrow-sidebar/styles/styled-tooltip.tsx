import type { TooltipProps } from "@mui/material";
import { Tooltip, styled, tooltipClasses } from "@mui/material";

type TProps = {
  marginLeft?: string;
} & TooltipProps;

export const StyledTooltip = styled(
  ({ className, marginLeft, ...props }: TProps) => (
    <Tooltip {...props} classes={{ popper: className }} />
  )
)<TProps>(({ theme, marginLeft = "0px" }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: theme.palette.common.white,
    color: "rgba(0, 0, 0, 0.87)",
    boxShadow: theme.shadows[1],
    fontSize: 11,
    margin: 0,
    marginLeft: "18px !important",
    padding: 0,
  },
}));
