import type { TSidebarItemsProps } from "../types";
import { NarrowSidebarItem } from "./narrow-sidebar";
import { WideSidebarItem } from "./wide-sidebar ";

export const SidebarItems = ({
  items,
  isNarrow,
  ...props
}: TSidebarItemsProps) => (
  <>
    {items.map((item, idx) =>
      isNarrow ? (
        <NarrowSidebarItem {...item} {...props} key={item.menuTitle.text} />
      ) : (
        <WideSidebarItem {...item} {...props} key={item.menuTitle.text} />
      )
    )}
  </>
);
