import type { ReactElement } from "react";
import type { TLinkProps } from "shared/ui/atoms";

export type TSidebarItem = {
  menuTitle: {
    Icon?: ReactElement;
    text: string;
    to?: string;
  };
  menuItems?: TLinkProps[];
};

export type TSidebarItemProps = TSidebarItem;

export type TSidebarItemsProps = {
  items: TSidebarItem[];
  isNarrow?: boolean;
};

export type TSidebarProps = {
  items: TSidebarItemProps[];
  isNarrow?: boolean;
};
