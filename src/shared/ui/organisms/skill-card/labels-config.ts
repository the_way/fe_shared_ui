export const LABELS: { [index: string]: string } = {
  1: "Elementary",
  2: "Intermediate",
  3: "Advanced",
  4: "Proficiency",
};
