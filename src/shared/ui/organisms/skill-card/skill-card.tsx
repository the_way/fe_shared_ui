import { Rating } from "shared/ui/atoms/rating";
import { Body1, Card, TCard } from "shared/ui/molecules";
import { Grid } from "src/remote";
import { LABELS } from "./labels-config";

type TProps = {
  skillName: string;
  rating: 0 | 1 | 2 | 3 | 4;
  collapseData?: TCard["collapseData"];
  /**
   * @default true
   */
  shouldShowLabel?: boolean;
} & Partial<Pick<TCard, "isOpenCollapse" | "collapseChildren">>;

export const SkillCard = ({
  skillName,
  rating,
  collapseData,
  isOpenCollapse,
  collapseChildren,
  shouldShowLabel = true,
}: TProps) => {
  return (
    <Card
      isOpenCollapse={isOpenCollapse}
      collapseData={collapseData}
      collapseChildren={collapseChildren}
    >
      <Grid
        xs={{ gridTemplateColumns: "1fr" }}
        sm={{ gridTemplateColumns: "180px 1fr" }}
      >
        <Body1>{skillName}</Body1>
        <Rating LABELS={shouldShowLabel ? LABELS : {}} value={rating} />
      </Grid>
    </Card>
  );
};
