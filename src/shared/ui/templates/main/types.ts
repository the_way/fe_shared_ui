import type { MemoExoticComponent, MouseEvent, ReactNode } from "react";
import type { TSidebarProps, TSidebarItem } from "shared/ui";

export type TMainProps = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window?: () => Window;
  children: ReactNode;
  navItems?: {
    title: string;
    to: string;
  }[];
  label: string;
  Sidebar: MemoExoticComponent<
    ({ items, ...props }: TSidebarProps) => JSX.Element
  >;
  sidebarData: TSidebarItem[];
  onLangToggleChange: (event: MouseEvent<HTMLElement>, lang: string) => void;
};
