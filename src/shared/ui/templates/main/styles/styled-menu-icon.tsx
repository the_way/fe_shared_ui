import MenuIcon from "@mui/icons-material/Menu";
import { styled } from "@mui/material";

export const StyledMenuIcon = styled(MenuIcon)(({ theme }) => ({
  color: theme.palette.primary.main,
}));
