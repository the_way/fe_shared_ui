import { IconButton, styled } from "@mui/material";

export const StyledIconButton = styled(IconButton)(({ theme }) => ({
  mr: 2,
  [theme.breakpoints.up("sm")]: {
    zIndex: -1,
    visibility: "hidden",
  },
}));
