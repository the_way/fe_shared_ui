import { AppBar, styled } from "@mui/material";

export const StyledAppBar = styled(AppBar)(({ theme }) => ({
  // [theme.breakpoints.up("sm")]: {
  //   display: "none",
  // },
  backgroundColor: theme.palette.grey[200],
})) as typeof AppBar;
