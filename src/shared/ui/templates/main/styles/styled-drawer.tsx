import { Drawer, styled } from "@mui/material";
import { SIDEBAR_WIDTH } from "shared/ui";

export const StyledDrawer = styled(Drawer)(({ theme }) => ({
  display: "block",
  [theme.breakpoints.up("sm")]: {
    display: "none",
  },
  "& .MuiDrawer-paper": {
    boxSizing: "border-box",
    width: SIDEBAR_WIDTH,
  },
}));
