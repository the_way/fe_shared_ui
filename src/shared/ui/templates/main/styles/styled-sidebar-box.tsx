import { Box, styled } from "@mui/material";

export const StyledSidebarBox = styled(Box)(({ theme }) => ({
  position: "fixed",
  top: theme.spacing(7),
  width: "70px",
  [theme.breakpoints.down("sm")]: {
    display: "none",
  },
}));
