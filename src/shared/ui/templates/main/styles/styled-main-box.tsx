import { Box, styled } from "@mui/material";

export const StyledMainContainer = styled(Box)(({ theme }) => ({
  width: "100%",
  backgroundColor: theme.palette.grey[200],
  display: "grid",
}));

export const StyledMainBox = styled(Box)(({ theme }) => ({
  display: "grid",
  gridTemplateColumns: "1fr",
  position: "absolute",

  [theme.breakpoints.down("sm")]: {
    width: "100%",
    left: 0,
    top: "45px",
    gridTemplateColumns: "1fr",
  },

  [theme.breakpoints.up("sm")]: {
    width: "100%",
    maxWidth: "1920px",
    left: "95px",
    top: "45px",
    gridTemplateColumns: "calc(100vw - 90px)",
  },

  maxWidth: "1920px",
}));
