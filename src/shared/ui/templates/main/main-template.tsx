import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import { useEffect, useMemo, useRef } from "react";
import { useState } from "react";

import {
  StyledAppBar,
  StyledIconButton,
  StyledMainBox,
  StyledMainContainer,
  StyledSidebarBox,
  StyledDrawer,
} from "./styles";
import type { TMainProps } from "./types";
import { useLocation } from "react-router-dom";
import { ToggleGroup, Grid } from "shared/ui";
import { StyledMenuIcon } from "./styles/styled-menu-icon";

export const MainTemplate = ({
  window: propWindow,
  children,
  label,
  Sidebar,
  sidebarData,
  onLangToggleChange,
}: TMainProps) => {
  const [mobileOpen, setMobileOpen] = useState(false);
  const mainBoxRef = useRef<HTMLDivElement>(null);
  const [mainBoxHeight, setMainBoxHeight] = useState(0);
  const location = useLocation();

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const drawer = useMemo(
    () => <Sidebar items={sidebarData} isNarrow={false} />,
    []
  );

  const container =
    propWindow !== undefined ? () => propWindow().document.body : undefined;

  useEffect(() => {
    if (mainBoxRef.current?.offsetHeight) {
      setMainBoxHeight(mainBoxRef.current?.offsetHeight + 100);
    }
    // TODO: (AM) добавить отслеживание изменения размера экрана
  }, [location.pathname]);

  return (
    <Box display="flex" width="100%" height={mainBoxHeight || "100vh"}>
      <StyledAppBar component="nav">
        <Grid gridTemplateColumns="max-content 1fr" padding={1}>
          <StyledIconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
          >
            <StyledMenuIcon />
          </StyledIconButton>
          <Box justifySelf="end">
            <ToggleGroup
              onChange={onLangToggleChange}
              size="small"
              data={[
                { label: "EN", value: "en" },
                { label: "RU", value: "ru" },
              ]}
              shouldEnforceValueSet
              exclusive
            ></ToggleGroup>
          </Box>
        </Grid>
      </StyledAppBar>
      <Box component="nav">
        <StyledDrawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          {drawer}
        </StyledDrawer>
      </Box>
      <StyledMainContainer component="main">
        <Toolbar />
        <StyledSidebarBox>
          <Sidebar items={sidebarData} isNarrow />
        </StyledSidebarBox>
        <StyledMainBox ref={mainBoxRef}>
          <Box>{children}</Box>
        </StyledMainBox>
      </StyledMainContainer>
    </Box>
  );
};
