import { createTheme } from "@mui/material/styles";

export let theme = createTheme({
  typography: {
    h1: { fontSize: "3rem" },
    h2: { fontSize: "2.5rem" },
    h3: { fontSize: "2.25rem" },
    h4: { fontSize: "2rem" },
    h5: { fontSize: "1.75rem" },
    h6: { fontSize: "1.5rem" },
    subtitle1: { fontSize: "1.75rem" },
    subtitle2: { fontSize: "1.5rem" },
    body1: { fontSize: "1.25rem" },
    body2: { fontSize: "1rem" },
    caption: { fontSize: "0.75rem" },
  },
});
