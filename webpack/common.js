require("dotenv").config();
const path = require("path");
// const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");
const Dotenv = require("dotenv-webpack");
const { srcPath } = require("./path");
const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");

module.exports = {
  entry: "./src/app/index",
  // при auto react-router не находит страницы

  resolve: {
    alias: {
      app: path.join(srcPath, "app"),
      processes: path.join(srcPath, "processes"),
      pages: path.join(srcPath, "pages"),
      widgets: path.join(srcPath, "widgets"),
      features: path.join(srcPath, "features"),
      entities: path.join(srcPath, "entities"),
      shared: path.join(srcPath, "shared"),
    },
    // идёт в порядке приоритета, что первее грузить при импорте
    extensions: [".ts", ".tsx", ".js"],
    plugins: [new TsconfigPathsPlugin()],
  },

  module: {
    rules: [
      {
        test: /\.svg$/,
        use: '@svgr/webpack',
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        type: 'asset/resource',
      },
      {
        test: /\.tsx?$/,
        loader: "babel-loader",
        exclude: /node_modules/,
        options: {
          presets: [
            [
              "@babel/preset-react",
              // без этого - ошибка 'react isn't defined
              {
                runtime: "automatic",
              },
            ],
            "@babel/preset-typescript",
          ],
        },
      },
    ],
  },
  plugins: [new Dotenv()],
};
