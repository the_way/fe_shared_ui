require("dotenv").config();
const path = require("path");
const { merge } = require("webpack-merge");
const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CircularDependencyPlugin = require("circular-dependency-plugin");
const ModuleFederationPlugin =
  require("webpack").container.ModuleFederationPlugin;
const ReactRefreshWebpackPlugin = require("@pmmmwh/react-refresh-webpack-plugin");
const deps = require("../package.json").dependencies;

const common = require("./common.js");
const commonModuleFederationConfig = require("./mf-config.js");
const { rootPath, staticPath, distPath, srcPath } = require("./path");

const {
  DEV_API = "http://localhost",
  FE_SHARED_UI_PORT = 8082,
  FE_AUTH_PORT = 8083,
  FE_HOST_PORT = 8084,
} = process.env;

module.exports = merge(common, {
  mode: "development",
  output: {
    publicPath: `${DEV_API}:${FE_SHARED_UI_PORT}/`,
  },
  // Choose a style of source mapping to enhance the debugging process. These values can affect build and rebuild speed dramatically.
  // Recommended choice for development builds with maximum performance.
  devtool: "eval",

  resolve: {
    symlinks: false,
    cacheWithContext: false,
  },

  cache: {
    type: "filesystem",
    cacheDirectory: path.join(rootPath, ".webpack_cache"),
    buildDependencies: {
      config: [__filename],
    },
  },

  stats: "errors-only",

  devServer: {
    port: FE_SHARED_UI_PORT,
    historyApiFallback: true,
  },
  plugins: [
    new ForkTsCheckerWebpackPlugin(),
    new CircularDependencyPlugin({
      exclude: /a\.js|node_modules/,
      include: /dir/,
      failOnError: true,
      allowAsyncCycles: false,
      cwd: process.cwd(),
    }),
    new HtmlWebpackPlugin({
      // Passing true will add it to the head/body depending on the scriptLoading option.
      inject: true,
      template: "./src/index.html",
    }),
    new ModuleFederationPlugin({
      ...commonModuleFederationConfig,
      remotes: {
        host: `host@${DEV_API}:${FE_HOST_PORT}/remoteEntry.js`,
        auth: `auth@${DEV_API}:${FE_AUTH_PORT}/remoteEntry.js`,
      },
    }),
  ],
  optimization: {
    // не работает с WMF
    // runtimeChunk: true,

    removeAvailableModules: false,
    removeEmptyChunks: false,
    splitChunks: false,
  },
});
