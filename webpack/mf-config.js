const deps = require("../package.json").dependencies;

const commonModuleFederationConfig = {
  name: "shared_ui",
  filename: "remoteEntry.js",
  shared: {
    ...deps,
    react: {
      singleton: true,
      requiredVersion: deps.react,
    },
    "react-dom": {
      singleton: true,
      requiredVersion: deps["react-dom"],
    },
    "react-router-dom": {
      requiredVersion: deps["react-router-dom"],
      singleton: true,
    },
  },
  exposes: {
    "./remote": "./src/remote",
  },
};

module.exports = commonModuleFederationConfig;
